This project is to implement Atlassian Automation Exercise. I built a simple framework that could be reused later.

##Pre-requites##
* IntelliJ IDEA 7 or higher or Eclipse IDE
* TestNG
* JDK
* Selenium 2 (jar files are already included in Libraries folder)
* Firefox version 33 or older

## Set up TestNG ##
* TestNG is supported natively in IntelliJ IDEA 7 or higher.
* If you're using eclipse, please see the guidelines at http://testng.org/doc/download.html 

##Tests Description##
### The automation test script will verify: ###
* New issues can be created.
* Existing issues can be updated.
* Existing issues can be found via JIRA’s search.

##How to run test script##
### Using IntelliJ IDEA: ###
* Under TestScript folder, right click on each test script file and choose option to run test script if you wish to run each script
* Under TestNG folder, right click on IssueProcessChecking.xml and choose to run all test scripts
### Using Eclipse ###
* Under TestNG folder, right click on IssueProcessChecking.xml and choose to run all test scripts using TestNG

##Test Scripts##
* TestCreateNewIssue - This script creates new issue, verifies if issue is created then modifies it.
* TestSearchExistingIssue - This script searches for existing issue then modifies it

##Issues##
* The test site loads very slowly, it could be my internet connection's problem.
* The Search menu is always available to click even when issue is being created. So I had to use Thread.sleep to wait for the create issue process to finish.
* The explicit wait is set to wait for a maximum of 300 seconds, if the site is too slow, the script will fail

##Assumptions made##
The issue is searched by summary

##Contact##
All the scripts are run with passed status. If you run the tests and they fails, please contact me.