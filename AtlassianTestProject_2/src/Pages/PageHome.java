package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by lphan7 on 10/29/14.
 */
public class PageHome {
    private WebDriver driver;

    public PageHome(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(linkText = "Log In")
    private WebElement logInBtn;

    public PageLogin ClickLoginBtn(){
        logInBtn.click();
        return new PageLogin(driver);
    }

}
