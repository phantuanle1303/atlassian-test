package Pages;

import Utilities.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import TestData.TestData;

import java.lang.reflect.Field;
import java.util.List;

public class PageCreateIssue {
    protected WebDriver driver;

    //Summary
    @FindBy(id = "summary")
    protected WebElement summary;


    //issue type Field
    @FindBy(id="issuetype-field")
    protected WebElement issueTypeField;

    //priority
    @FindBy(id="priority-field")
    protected WebElement priority;


    //description
    @FindBy(id ="description")
    protected WebElement description;


    //Create issue button
    @FindBy(id= "create-issue-submit")
    protected WebElement createBtn;

    //Init Elements on Page
    public PageCreateIssue(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //Select an issue Type
    public PageCreateIssue SelectIssue(String issueType){
        Utilities.WaitForElementToBeClickable(driver, issueTypeField);
        this.issueTypeField.click();
        WebElement issueTypeSuggestions = driver.findElement(By.id("issuetype-suggestions"));
        List<WebElement> issueTypeList = issueTypeSuggestions.findElements(By.tagName("a"));
        for(WebElement issueTypeItem:issueTypeList){
            if(issueTypeItem.getAttribute("title").equalsIgnoreCase(issueType)){
                issueTypeItem.click();
                break;
            }
        }
        return this;
    }

    //Select priority
    public PageCreateIssue SelectPriority(String priority){
        Utilities.WaitForElementToBeClickable(driver, this.priority);
        this.priority.click();
        WebElement prioritySuggestions = driver.findElement(By.id("priority-suggestions"));
        List<WebElement> issueTypeList = prioritySuggestions.findElements(By.tagName("a"));
        for(WebElement issueTypeItem:issueTypeList){
            if(issueTypeItem.getAttribute("title").equalsIgnoreCase(priority)){
                issueTypeItem.click();
                break;
            }
        }
        return this;
    }

    public PageCreateIssue EnterSummary(String summary){
        Utilities.WaitForElementToBeClickable(driver, this.summary);
        if(this.summary.getText().equals(summary)==false){
            this.summary.clear();
            this.summary.sendKeys(summary);
        }
        return this;
    }

    public PageCreateIssue EnterDescription(String description){
        Utilities.WaitForElementToBeClickable(driver, this.description);
        if(this.description.getText().equals(description)==false){
            this.description.clear();
            this.description.sendKeys(description);
        }
        return this;
    }

    //Click Create button
    public PageDashBoard ClickCreateBtn(){
        createBtn.click();
        return new PageDashBoard(driver);
    }
    public PageDashBoard CreateIssue(TestData testData) throws InterruptedException {
        SelectIssue(testData.issue.issueType);
        EnterSummary(testData.issue.summary);
        SelectPriority(testData.issue.priority);
        EnterDescription(testData.issue.description);
        return ClickCreateBtn();
    }
}
