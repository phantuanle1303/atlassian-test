package Pages;

import Utilities.Utilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class PageDashBoard {
    protected WebDriver driver;
    @FindBy(id = "create_link")
    private WebElement createBtn;


    @FindBy(id = "find_link")
    private WebElement issueMenu;

    @FindBy(id="issues_new_search_link_lnk")
    private WebElement searchForIssue;

    public PageDashBoard(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public PageCreateIssue ClickCreateBtn(){
        Utilities.WaitForElementToBeClickable(driver, createBtn);
        createBtn.click();
        return new PageCreateIssue(driver);
    }

    public PageSearchIssue ClickSearchForIssue(){
        Utilities.WaitForElementToBeClickable(driver, this.issueMenu).click();
        Utilities.WaitForElementToBeClickable(driver, this.searchForIssue).click();
        return new PageSearchIssue(driver);
    }
}
