package Pages;

import Utilities.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class PageSearchIssue extends PageDashBoard{
    public PageSearchIssue(WebDriver driver) {
        super(driver);
    }

    @FindBy(id="searcher-query")
    private WebElement searchQuery;

    @FindBy(xpath = "/html/body/div[1]/section/div[1]/div[4]/div/form/div[1]/div[1]/div[1]/div[1]/div/div[1]/ul/li[7]/button")
    private WebElement searchBtn;

    //Click Edit Button
    public void ClickEditBtn(){
        List<WebElement> editBtnList = driver.findElements(By.id("edit-issue"));
        for(WebElement editBtn: editBtnList){
            if(editBtn.isDisplayed()){
                editBtn.click();
                break;
            }
        }
    }

    //Enter Seacrh Query
    public void EnterSeachQuery(String searchString) throws InterruptedException {
        Utilities.WaitForElementToBeClickable(driver, searchQuery);
        searchQuery.sendKeys(searchString);
        Utilities.WaitForElementToBeClickable(driver, searchBtn);
        searchBtn.click();
        Thread.sleep(3000);
    }




}
