package Pages;

import TestData.TestData;
import Utilities.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class PageEditIssue extends PageCreateIssue{
    public PageEditIssue(WebDriver driver) {
        super(driver);
    }

    @FindBy(id="edit-issue-submit")
    private WebElement updateIssueBtn;

    //Select an issue Type
    public PageEditIssue SelectIssue(String issueType){
        Utilities.WaitForElementToBeClickable(driver, issueTypeField);
        this.issueTypeField.click();
        WebElement issueTypeSuggestions = driver.findElement(By.id("issuetype-suggestions"));
        List<WebElement> issueTypeList = issueTypeSuggestions.findElements(By.tagName("a"));
        for(WebElement issueTypeItem:issueTypeList){
            if(issueTypeItem.getAttribute("title").equalsIgnoreCase(issueType)){
                issueTypeItem.click();
                break;
            }
        }
        return this;
    }

    //Select priority
    public PageEditIssue SelectPriority(String priority){
        Utilities.WaitForElementToBeClickable(driver, this.priority);
        this.priority.click();
        WebElement prioritySuggestions = driver.findElement(By.id("priority-suggestions"));
        List<WebElement> issueTypeList = prioritySuggestions.findElements(By.tagName("a"));
        for(WebElement issueTypeItem:issueTypeList){
            if(issueTypeItem.getAttribute("title").equalsIgnoreCase(priority)){
                issueTypeItem.click();
                break;
            }
        }
        return this;
    }

    public PageEditIssue EnterSummary(String summary){
        Utilities.WaitForElementToBeClickable(driver, this.summary);
        if(this.summary.getText().equals(summary)==false){
            this.summary.clear();
            this.summary.sendKeys(summary);
        }
        return this;
    }

    public PageEditIssue EnterDescription(String description){
        Utilities.WaitForElementToBeClickable(driver, this.description);
        if(this.description.getText().equals(description)==false){
            this.description.clear();
            this.description.sendKeys(description);
        }
        return this;
    }

    public PageSearchIssue ModifyIssue(TestData testData){
        SelectIssue(testData.issue.issueType);
        EnterSummary(testData.issue.summary);
        SelectPriority(testData.issue.priority);
        EnterDescription(testData.issue.description);
        return ClickUpdateBtn();
    }

    public boolean isDataUpdated(String updatedField, String checkString){
        if(updatedField.equalsIgnoreCase("summary")){
            Utilities.WaitForElementToBeClickable(driver,summary);
            if(summary.getText().equals(checkString))
                return true;
        }
        if(updatedField.equalsIgnoreCase("issuetype")){
            Utilities.WaitForElementToBeClickable(driver,issueTypeField);
            if(issueTypeField.getText().equals(checkString))
                return true;
        }

        if(updatedField.equalsIgnoreCase("priority")){
            Utilities.WaitForElementToBeClickable(driver,priority);
            if(priority.getText().equals(checkString))
                return true;
        }

        if(updatedField.equalsIgnoreCase("description")){
            Utilities.WaitForElementToBeClickable(driver,description);
            if(description.getText().equals(checkString))
                return true;
        }
        return false;
    }

    private PageSearchIssue ClickUpdateBtn(){
        Utilities.WaitForElementToBeClickable(driver, updateIssueBtn);
        updateIssueBtn.click();
        return new PageSearchIssue(driver);
    }
}
