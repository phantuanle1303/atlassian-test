package Pages;

import TestData.TestData;
import Utilities.Utilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageLogin {
    private WebDriver driver;

    //Username
    @FindBy(id="username")
    private WebElement username;

    //Password
    @FindBy(id="password")
    private WebElement password;

    //Log In button
    @FindBy(id = "login-submit")
    private WebElement logInBtn;

    //init Login page
    public PageLogin(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //Enter Log In info
    public PageDashBoard EnterLoginInfo(TestData testData){
        EnterUsername(testData.testEnvironment.username);
        EnterPassword(testData.testEnvironment.password);
        return ClickLoginBtn();

    }

    public PageLogin EnterPassword(String password){
        Utilities.WaitForElementToBeClickable(driver, this.password);
        this.password.sendKeys(password);
        return this;
    }

    public PageLogin EnterUsername(String username){
        Utilities.WaitForElementToBeClickable(driver, this.username);
        this.username.sendKeys(username);
        return this;
    }

    //click Login
    public PageDashBoard ClickLoginBtn(){
        logInBtn.click();
        return new PageDashBoard(driver);
    }
}
