package TestData;

import Utilities.Utilities;
import org.w3c.dom.NodeList;

public class Issue{
    public String project;
    public String issueType;
    public String priority;
    public String summary;
    public String description;

    public Issue(NodeList nodeList){
       Utilities.getTestData(nodeList, this);
    }
}
