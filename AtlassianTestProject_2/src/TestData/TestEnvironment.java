package TestData;

import Utilities.Utilities;
import org.w3c.dom.NodeList;

public class TestEnvironment {
    public String TestURL;
    public String username;
    public String password;

    public TestEnvironment(NodeList nodeList){
        Utilities.getTestData(nodeList,this);
    }
}
