package TestData;


import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.lang.reflect.Field;
import java.util.Properties;

public class TestData {
    public TestEnvironment testEnvironment;
    public Issue issue;

    public TestData(String xmlPath){
        try{
            File xmlTestData = new File(xmlPath);
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            org.w3c.dom.Document document = documentBuilder.parse(xmlTestData);
            document.getDocumentElement().normalize();

            NodeList testEnvironmentNode = document.getElementsByTagName("TestEnvironment");
            testEnvironment = new TestEnvironment(testEnvironmentNode);

            NodeList issueNode = document.getElementsByTagName("Issue");
            issue = new Issue(issueNode);
        }
        catch(Exception ex){}
    }
}
