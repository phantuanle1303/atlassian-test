package TestScript;

import Pages.PageCreateIssue;
import Pages.PageDashBoard;
import Pages.PageEditIssue;
import Pages.PageSearchIssue;
import Utilities.Utilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

import static org.testng.Assert.assertTrue;

public class TestSearchExistingIssue extends TestBase {
    @BeforeMethod
    //Set up Test
    public void setUp(Method method){
        super.TestInitialize(method);
    }

    @AfterMethod
    //Clean up Test
    public void cleanUp(){
        super.TestCleanUp();
    }

    @Test(description = "login")
    public void TestSearchIssue() throws InterruptedException {
        try {
            //Variable Declaration
            //Page Declaration
            PageDashBoard pageDashBoard = new PageDashBoard(driver);
            PageSearchIssue pageSearchIssue = new PageSearchIssue(driver);
            PageEditIssue pageEditIssue = new PageEditIssue(driver);

            //Login
            CommonScripts.Login(driver, testData);

            //Click Search for issue
            pageDashBoard.ClickSearchForIssue();

            //Search for issue
            pageSearchIssue.EnterSeachQuery(testData.issue.summary);
            //Checkpoint 1 : search issue
            //Check if issue is created
            checkPointResult = Utilities.isIssueCreated(driver,testData.issue.summary);
            if (checkPointResult == false) {
                testResult = false;
                comments += "Issue was not created. ";
                assertTrue(testResult);
            }

            //End check point 1
            //Check point 2: If issue exists, modify it
            //Issue was created. Continue Test
            else{
                //Select issue
                Utilities.SelectIssue(driver, testData.issue.summary);

                //Select Edit button to begin modify issue
                pageSearchIssue.ClickEditBtn();

                //Modify description
                testData.issue.description += Utilities.getDateTime();

                //Edit description
                pageEditIssue.ModifyIssue(testData);

                //Wait for modifying issue process to finish
                Thread.sleep(6000);

                //Click Edit button to check description
                pageSearchIssue.ClickEditBtn();

                //Check if description is actually changed
                checkPointResult = pageEditIssue.isDataUpdated("description", testData.issue.description);
                if(checkPointResult == false){
                    testResult = false;
                    comments+= "Data are not updated";
                    checkPointResult = true;
                }
                //End checkpoint 2
                //Print comments
                System.out.println(comments);

                //Assert Test
                assertTrue(testResult);

            }
        }
        catch(Exception ex){}

    }

}
