package TestScript;
import Pages.PageDashBoard;
import Pages.PageCreateIssue;
import Pages.PageEditIssue;
import Pages.PageSearchIssue;
import Utilities.Utilities;
import org.testng.annotations.*;

import java.lang.reflect.Method;

import static org.testng.Assert.assertTrue;

public class TestCreateNewIssue extends TestBase {
    @BeforeMethod
    //Set up Test
    public void setUp(Method method){
        super.TestInitialize(method);
    }

    @AfterMethod
    //Clean up Test
    public void cleanUp(){
        super.TestCleanUp();
    }

    @Test(description = "login")
    public void TestCreateIssue() throws InterruptedException {
        try {
            //Variable Declaration
            testData.issue.summary = testData.issue.summary + Utilities.getDateTime();
            //Page Declaration
            PageDashBoard pageDashBoard = new PageDashBoard(driver);
            PageCreateIssue pageCreateIssue = new PageCreateIssue(driver);
            PageSearchIssue pageSearchIssue = new PageSearchIssue(driver);
            PageEditIssue pageEditIssue = new PageEditIssue(driver);

            //Login
            CommonScripts.Login(driver, testData);

            //Check point 1 : Create Issue
            //Wait for Create Btn to display
            pageDashBoard.ClickCreateBtn();

            //Enter issue information
            pageCreateIssue.CreateIssue(testData);
            //End Check point 1

            //Check point 2 : Search for issue and check if issue was created
            //Click Search for issue
            pageDashBoard.ClickSearchForIssue();

            //Search for issue
            pageSearchIssue.EnterSeachQuery(testData.issue.summary);

            //Check if issue is created
            checkPointResult = Utilities.isIssueCreated(driver,testData.issue.summary);
            if (checkPointResult == false) {
                testResult = false;
                comments += "Issue was not created. ";
                assertTrue(testResult);
            }

            //End check point 2
            //Check point 3: If issue is created, modified it
            //Issue was created. Continue Test
            else{
                //Select issue
                Utilities.SelectIssue(driver,testData.issue.summary);

                //Select Edit button to begin modify issue
                pageSearchIssue.ClickEditBtn();

                //Modify description
                testData.issue.description += Utilities.getDateTime();

                //Edit description
                pageEditIssue.ModifyIssue(testData);

                //Wait for modifying issue process to finish
                Thread.sleep(6000);

                //Click Edit button to check description
                pageSearchIssue.ClickEditBtn();

                //Check if description is actually changed
                checkPointResult = pageEditIssue.isDataUpdated("description", testData.issue.description);
                if(checkPointResult == false){
                    testResult = false;
                    comments+= "Data are not updated";
                    checkPointResult = true;
                }
                //End checkpoint 3
                //Print comments
                System.out.println(comments);

                //Assert Test
                assertTrue(testResult);

            }
        }
        catch(Exception ex){}

    }

}
