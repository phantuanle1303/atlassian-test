package TestScript;

import TestData.TestData;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.lang.reflect.Method;

public class TestBase {
    public static WebDriver driver;
    public TestData testData;
    public boolean testResult;
    public boolean checkPointResult;
    public String comments;

    public void TestInitialize(Method method){
        testData = new TestData("src/TestData/" + method.getName()+".xml");
        testResult = true;
        checkPointResult = true;
        comments = "";
        this.driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get(testData.testEnvironment.TestURL);
    }

    public void TestCleanUp(){
        driver.close();
        driver.quit();
    }
}
