package TestScript;

import Pages.PageLogin;
import TestData.TestData;
import Utilities.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CommonScripts {

    public static void Login(WebDriver driver, TestData testData){
        //Variable Declaration
        //WebDriverWait wait = new WebDriverWait(driver,180,500);
        //Page Declaration
        PageLogin pageLogin = new PageLogin(driver);

        //Wait until Login button is displayed then click it
        //wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Log In"))).click();
        Utilities.WaitForElementToBeClickable(driver, driver.findElement(By.linkText("Log In"))).click();

        //Enter account credential
        pageLogin.EnterLoginInfo(testData);

        //Log In
        pageLogin.ClickLoginBtn();
    }
}
