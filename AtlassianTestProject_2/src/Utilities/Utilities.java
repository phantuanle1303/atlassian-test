package Utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Utilities {
    public static WebElement WaitForElementToBeClickable(WebDriver driver, WebElement we){
        WebDriverWait wait = new WebDriverWait(driver,300,500);
        return wait.until(ExpectedConditions.elementToBeClickable(we));
    }

    public static WebElement WaitForElementToDisplay(WebDriver driver, WebElement we){
        WebDriverWait wait = new WebDriverWait(driver,300,500);
        return wait.until(ExpectedConditions.visibilityOf(we));
    }

    public static String getDateTime(){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat smf = new SimpleDateFormat("yyyy MM dd EEE HH mm ss");
        return smf.format(cal.getTime());
     }


    //Get Test Data from XML
    public static void getTestData(NodeList nodeList, Object obj) {
        List<Element> testData = getNodeInfo(nodeList);
        try {
            for (Element data : testData) {
                String tagName = data.getTagName().toLowerCase();
                for (Field f : obj.getClass().getDeclaredFields()) {
                    if (tagName.equals(f.getName().toLowerCase())) {
                        Object value = data.getTextContent();
                        if (f.getType().equals(boolean.class)) {
                            // f.setBoolean(obj, true);//data.getTextContent());
                            if (value.toString().toLowerCase().equals("yes")) {
                                value = true;
                            } else {
                                value = false;
                            }
                            f.set(obj, value);
                        } else {
                            value = decodeXML(value.toString());
                            f.set(obj, value);
                        }
                        break;
                    }
                }
            }
        } catch (Exception ex) {
        }
    }

    //get XMl Node Info
    public static List<Element> getNodeInfo(NodeList nodeList)
    {
        try{
            List<Element> testData = new ArrayList<Element>();
            for(int i = 0; i< nodeList.getLength(); i++){
                NodeList childNodeCollection = nodeList.item(i).getChildNodes();
                for(int j = 0; j< childNodeCollection.getLength(); j++){
                    Node node = childNodeCollection.item(j);
                    if(node.getNodeType() == Node.ELEMENT_NODE){
                        Element element = (Element)node;
                        testData.add(element);
                    }
                }
            }
            return testData;
        }
        catch (Exception e)
        {
            return null;
        }
    }

    public static String decodeXML(String xmlData){
        if(xmlData.contains("&amp;")){
            return xmlData = xmlData.replace("xmlData","&");
        }
        return xmlData;
    }

    //Check if issue is created
    public static boolean isIssueCreated(WebDriver driver,String searchQuery){
        boolean isCreated = false;
        WebElement issueList = Utilities.WaitForElementToBeClickable(driver,driver.findElement(By.className("issue-list")));
        List<WebElement> issuesList = issueList.findElements(By.tagName("li"));
        for(WebElement issue:issuesList){
            if(issue.getAttribute("title").equalsIgnoreCase(searchQuery)){
                isCreated = true;
                break;
            }
        }
        return isCreated;
    }

    //Select the issue
    public static void SelectIssue(WebDriver driver, String searchQuery){

        WebElement issueList = Utilities.WaitForElementToDisplay(driver, driver.findElement(By.className("issue-list")));
        List<WebElement> issuesList = issueList.findElements(By.tagName("li"));
//        for(WebElement issue:issuesList){
//            if(issue.getAttribute("title").equalsIgnoreCase(searchQuery)){
//                Utilities.WaitForElementToBeClickable(driver, issue.findElement(By.className("splitview-issue-link"))).click();
//
//            }
//        }
        for(int i = 0; i < issuesList.size(); i++){
            if(issuesList.get(i).getAttribute("title").equalsIgnoreCase(searchQuery)){
                WebElement issue = issueList.findElement(By.xpath("//ol[@class='issue-list']/li[" + (i + 1) +"]/a"));
                issue.click();
            }
        }
    }
}
